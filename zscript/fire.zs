// ------------------------------------------------------------
// Setting things on fire
// ------------------------------------------------------------
class ImmunityToFire:InventoryFlag{
	override void attachtoowner(actor user){
		super.attachtoowner(user);
		if(owner){
			actoriterator it=level.createactoriterator(-7677,"HDFire");
			actor fff;
			while(fff=it.next()){
				if(
					HDFire(fff)
					&&fff.target==owner
				){
					fff.destroy();
				}
			}
		}
	}
}
class HDFireEnder:InventoryFlag{
	default{
		inventory.maxamount 5;
	}
}
class HDFireDouse:InventoryFlag{
	default{
		inventory.maxamount 20;
	}
	override void DoEffect(){
		if(amount>0)amount--;
	}
}


//how to immolate
extend class HDActor{
	//A_Immolate(tracer,target);
	virtual void A_Immolate(
		actor victim,
		actor perpetrator,
		int duration=0,
		bool requireSight=false
	){
		if(victim&&victim.countinv("ImmunityToFire"))return;

		if (requireSight && victim && perpetrator && !perpetrator.CheckSight(victim, SF_IGNOREVISIBILITY))
		{
			return;
		}

		if(
			!victim
			||(
				perpetrator
				&&perpetrator.bdontharmspecies
				&&perpetrator.getspecies()==victim.getspecies()
			)
		){
			victim=spawn("PersistentDamager",self.pos,ALLOW_REPLACE);
			victim.target=perpetrator;
		}

		actor f=null;
		thinkeriterator fit=thinkeriterator.create("HDFire", STAT_DEFAULT);
		while(f=actor(fit.next(true))){
			if(f.target==victim){
				f.master=perpetrator;
				break;
			}
		}
		if(!f){
			f=victim.spawn("HDFire",victim.pos,ALLOW_REPLACE);
			f.target=victim;f.master=perpetrator;
			f.stamina=0;
		}

		if(duration<1)f.stamina+=random(40,80);
		else f.stamina+=duration;

		if(victim.player){
			f.changetid(-7677);
			victim.player.attacker=perpetrator;
		}
	}
}
//fire actor
class HDFire:IdleDummy{
	double halfrad,minz,maxz,lastheight;
	default{
		+bloodlessimpact
		obituary "%o was burned by %k.";
	}
	override void postbeginplay(){
		super.postbeginplay();
		if(target){
			stamina=target.ApplyDamageFactor("hot",stamina);
			if(target.player || target is "HDPlayerCorpse"){
				changetid(-7677);
				stamina=int(max(1,hd_damagefactor*stamina));
			}
			if(!target.bshootable && stamina>20)stamina=20;
		}
		if(hd_debug)A_Log(string.format("fire duration \ci%i",stamina));
	}
	override void ondestroy(){
		if(PersistentDamager(target))target.destroy();
		super.ondestroy();
	}
	override void Tick(){
		if(isfrozen())return;
		if(accuracy>0){
			accuracy--;
			return;
		}

		//abort on certain immunities
		if(
			target
			&&(
				(
					!!target.player
					&&target.player.cheats&(CF_GODMODE2|CF_GODMODE)
				)
				||target.countinv("ImmunityToFire")
			)
		){
			destroy();return;
		}

		if(!master)master=self;
		if(!target){
			target=spawn("PersistentDamager",self.pos,ALLOW_REPLACE);
			target.target=master;
			if(stamina>20)stamina=20;
		}
		setorigin(target.pos,false);


		if(
			stamina<=0
			||target.countinv("HDFireEnder")
		){
			A_TakeFromTarget("HDFireEnder");
			spawn("HDSmoke",pos,ALLOW_REPLACE);
			destroy();
			return;
		}


		int wlvl=target.waterlevel;
		if(wlvl>1){
			if(wlvl<2)spawn("HDSmoke",pos,ALLOW_REPLACE);
			destroy();
			return;
		}


		target.bspecialfiredamage=false;
		target.bspawnsoundsource=true;

		//check if player or HD monster
		let tgt=HDPlayerPawn(target);
		if(tgt){
			if(tgt.playercorpse){
				target=tgt.playercorpse;
			}
			A_TakeFromTarget("PowerFrightener");
			IsMoving.Give(tgt,4);
			HDWeapon.SetBusy(target);
		}


		int ds=target.countinv("HDFireDouse");
		if(ds){
			target.A_TakeInventory("HDFireDouse",ds);
			stamina-=ds;
		}
		stamina--;


		accuracy=(clamp(random(3,int(30-stamina*0.1)),2,12));


		//set flame spawn point
		if(lastheight!=target.height){ //poll only height
			halfrad=max(4,target.radius*0.5);
			lastheight=target.height;
			minz=lastheight*0.4;
			maxz=max(lastheight*0.9,4);
		}

		//position and spawn flame
		setorigin(pos+(
				frandom(-halfrad,halfrad),
				frandom(-halfrad,halfrad),
				frandom(minz,maxz)
		),false);
		actor sp=spawn("HDFlameRed",pos,ALLOW_REPLACE);
		sp.vel+=target.vel;
		A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP,volume:0.4,attenuation:6.);


		//heat up the target
		target.A_GiveInventory("Heat",clamp(stamina,20,random(20,80)));
	}
	states{
	spawn:
		TNT1 A -1;
		stop;
	}
}




//an invisible actor that constantly damages anything it collides with
class PersistentDamager:HDActor{
	vector3 relpos;
	default{
		+noblockmap
		damagetype "hot";

		height 8;radius 8;
		stamina 8;
	}
	override void postbeginplay(){
		super.postbeginplay();
		if(master)relpos=self.pos-master.pos;
	}
	override void tick(){
		if(isfrozen())return;

		if(master)setorigin(master.pos+relpos,false);
		if(!(getage()&(1|2))){
			blockthingsiterator ccw=blockthingsiterator.create(self);
			while(ccw.next()){
				actor ccc=ccw.thing;
				if(
					ccc.bnodamage
					||!ccc.bshootable
					||ccc.pos.z<pos.z-ccc.height
					||!ccc.checksight(self,SF_IGNOREVISIBILITY)  //hope this doesn't bog things down too much
				)continue;
				stamina--;
				if(damagetype=="hot")HDF.Give(ccc,"Heat",stamina*10);
				ccc.damagemobj(self,target,stamina,damagetype);
			}
			stamina--;
			if(stamina<1){destroy();return;}
		}

		NextTic();
	}
	states{
	spawn:
		TNT1 A -1;
		stop;
	}
}



