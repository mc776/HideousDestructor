//-------------------------------------------------
// Medikit
//-------------------------------------------------
class PortableMedikit:HDPickup{
	default{
		//$Category "Items/Hideous Destructor/Supplies"
		//$Title "Medikit"
		//$Sprite "MEDIA0"

		-hdpickup.droptranslation
		inventory.pickupmessage "$PICKUP_MEDIKIT";
		inventory.icon "MEDIA0";
		scale 0.4;
		hdpickup.bulk ENC_MEDIKIT;
		tag "$TAG_MEDIKIT";
		hdpickup.refid HDLD_MEDIKIT;
		+inventory.ishealth
	}
	states{
	spawn:
		MEDI A -1;
		stop;
	use:
		TNT1 A 0{
			class<weapon> kittype=hd_nobleed?"HDMedikitterNoBleed":"HDMedikitter";
			if(
				!FindInventory(kittype)
				||player.cmd.buttons&BT_USE
			){
				let mdk=HDWeapon(spawn(kittype,pos));
				mdk.actualpickup(self,true);
				mdk=HDWeapon(spawn("PortableStimpack",pos));
				mdk.actualpickup(self,true);
				mdk=HDWeapon(spawn("SecondBlood",pos));
				mdk.actualpickup(self,true);
				A_TakeInventory("PortableMedikit",1);
			}else{
				A_Log(Stringtable.Localize("$MEDIKIT_USEUNWRAPPED"),true);
				if(HDWeapon.CheckDoHelpText(self))A_Log(Stringtable.Localize("$MEDIKIT_USEUNWRAPPEDHELP"),true);
			}
			if(!hdplayerpawn(self)||!hdplayerpawn(self).incapacitated)A_SelectWeapon(kittype);
			A_StartSound("weapons/pocket",9);
		}
		fail;
	}
}

enum MediNums{
	MEDIKIT_FLESHGIVE=5,
	MEDIKIT_MAXFLESH=42,
	MEDIKIT_NOTAPLAYER=MAXPLAYERS+1,

	MEDS_SECONDFLESH=1,
	MEDS_USEDON=2,
	MEDS_ACCURACY=3,
	MEDS_BLOOD=4,

	CHECKCOV_ONLYFULL=1,
	CHECKCOV_CHECKBODY=2,
	CHECKCOV_CHECKFACE=4,
}
class HDMedikitter:HDWoundFixer{
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	default{
		+inventory.invbar
		-nointeraction
		weapon.selectionorder 1001;
		weapon.slotnumber 9;
		scale 0.5;
		tag "$TAG_2NDFLESHAPP";
		hdweapon.refid HDLD_FINJCTR;
		inventory.icon "MEDIB0";
		inventory.pickupmessage "$PICKUP_OPENAPPLICATOR";
	}
	override string pickupmessage(){
		string msg=super.pickupmessage();
		if(weaponstatus[MEDS_USEDON]!=-1)msg=Stringtable.Localize("$PICKUP_USEDAPPLICATOR");
		return msg;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[MEDS_SECONDFLESH]=MEDIKIT_MAXFLESH;
		weaponstatus[MEDS_USEDON]=-1;
		patientname=Stringtable.Localize("$MEDIKIT_UNKNOWN");
	}
	override double weaponbulk(){
		return ENC_MEDIKIT;
	}
	override string,double getpickupsprite(){
		return (weaponstatus[MEDS_USEDON]<0)?"MEDIB0":"MEDIC0",0.5;
	}
	string patientname;
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		let ww=hdmedikitter(hdw);
		int of=0;
		let bwnd=hdbleedingwound.findbiggest(hpl);
		if(
			bwnd
			&&(weaponstatus[MEDS_USEDON]<0||weaponstatus[MEDS_USEDON]==hpl.playernumber())
		){
			of=clamp(int(bwnd.width*0.1),1,3);
			if(hpl.flip)of=-of;
		}
		sb.drawrect(-29,-17+of,2,6);
		sb.drawrect(-31,-15+of,6,2);

		int usedon=weaponstatus[MEDS_USEDON];
		if(usedon>=0){
			int upn=weaponstatus[MEDS_USEDON];
			string pn=
				upn>=0
				&&upn<MAXPLAYERS
				&&playeringame[upn]
				?players[upn].getusername()
				:patientname
			;
			sb.DrawString(sb.psmallfont,pn,(-53,-8),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT|sb.DI_TEXT_ALIGN_RIGHT,
				Font.CR_RED,scale:(0.3,0.5)
			);
			sb.drawimage(
				"BLUDB0",(-7,-12),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_VCENTER|sb.DI_ITEM_RIGHT,
				0.2+min(0.4,0.01*ww.weaponstatus[MEDS_BLOOD]),scale:(1.5,1.5)*(1+0.02*ww.weaponstatus[MEDS_BLOOD])
			);
		}

		int btn=hpl.player.cmd.buttons;
		if(!(btn&BT_FIREMODE)){
			sb.drawwepnum(ww.weaponstatus[MEDS_SECONDFLESH],MEDIKIT_MAXFLESH);

			let targetwound=ww.targetwound;
			if(!!targetwound){
				double tgtwsc=1.4+targetwound.width*0.1;
				double tgtwa=0;
				if(tgtwsc>3.){
					tgtwa=3.-tgtwsc;
					tgtwsc=3.;
				}
				sb.drawimage(
					"BLUDC0",(-15,!!targetwound.width&&hpl.flip?-8:-7),
					sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT,
					0.01+targetwound.depth*0.1+tgtwa,scale:(1,1)*tgtwsc
				);
			}
		}
	}
	override string gethelptext(){
		LocalizeHelp();
		int usedon=weaponstatus[MEDS_USEDON];
		return
		LWPHELP_RELOAD..StringTable.Localize("$SFAWH_RELOAD")
		..LWPHELP_INJECTOR
		..StringTable.Localize("$SFAWH_WPRESS")
		..StringTable.Localize("$SFAWH_NOTHING")..WEPHELP_RGCOL..StringTable.Localize("$SFAWH_FIRE")
		.."  "..LWPHELP_ZOOM..StringTable.Localize("$SFAWH_ZOOM")
		.."  "..LWPHELP_FIREMODE..StringTable.Localize("$SFAWH_FMODE")
		;
	}
	action void A_MedikitReady(){
		A_WeaponReady(WRF_NOFIRE|WRF_ALLOWUSER1|WRF_ALLOWUSER3);
		if(!player)return;
		int bt=player.cmd.buttons;

		if(
			invoker.icon==invoker.default.icon
			&&invoker.weaponstatus[MEDS_USEDON]>=0
		)invoker.icon=texman.checkfortexture("MEDIC0",TexMan.Type_MiscPatch);

		//don't do the other stuff if holding reload
		//LET THE RELOAD STATE HANDLE EVERYTHING ELSE
		if(bt&BT_RELOAD){
			setweaponstate("reload");
			return;
		}

		//wait for the player to decide what they're doing
		if(bt&BT_ATTACK&&bt&BT_ALTATTACK)return;

		//just gotta let go
		if(!(bt&(BT_ATTACK|BT_ALTATTACK)))invoker.targetwound=null;

		//..of the idea of bleeding altogether???
		else if(hd_nobleed){
			A_GiveInventory("HDMedikitterNoBleed");
			invoker.destroy();
			HDMedikitterNoBleed.ReplaceSpares(self);
			
			//no fancy delayed select since this is inside the ready state
			if(
				!hdplayerpawn(self)
				||!hdplayerpawn(self).incapacitated
			)A_SelectWeapon("HDMedikitterNoBleed");

			return;
		}

		//use on someone else
		if(bt&BT_ALTATTACK){
			if(
				(bt&BT_FIREMODE)
				&&!(bt&BT_ZOOM)
			)setweaponstate("diagnoseother");
			else if(invoker.weaponstatus[MEDS_SECONDFLESH]<1){
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOSUTURES"));
				setweaponstate("nope");
			}else setweaponstate("fireother");
			return;
		}

		//self
		if(bt&BT_ATTACK){
			invoker.bwimpy_weapon=false;  //uncloak

			//radsuit, etc. blocks everything
			let blockinv=HDWoundFixer.CheckCovered(self,CHECKCOV_ONLYFULL);
			if(blockinv){
				A_TakeOffFirst(blockinv.gettag());
				setweaponstate("nope");
				return;
			}
			if(pitch<min(player.maxpitch,80)){
				//move downwards
				let hdp=hdplayerpawn(self);
				if(hdp)hdp.gunbraced=false;
				A_MuzzleClimb(0,5,0,5);
			}else{
				bool scanning=bt&BT_FIREMODE;
				//armour blocks everything except scan
				let blockinv=HDWoundFixer.CheckCovered(self,CHECKCOV_CHECKBODY);
				if(
					!scanning
					&&blockinv
				){
					A_TakeOffFirst(blockinv.gettag());
					setweaponstate("nope");
					return;
				}
				//diagnose
				if(scanning){
					setweaponstate("diagnose");
					return;
				}
				//act upon flesh
				if(invoker.weaponstatus[MEDS_SECONDFLESH]<1){
					A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOSUTURES"));
					setweaponstate("nope");
					return;
				}
				if(bt&BT_ZOOM){
					//treat burns
					let a=HDPlayerPawn(self);
					if(a){
						if(a.burncount<1){
							A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOBURNS"));
							setweaponstate("nope");
						}else setweaponstate("patchburns");
						return;
					}
				}else{
					//treat wounds
					if(!hdbleedingwound.findbiggest(self,HDBW_FINDPATCHED)){
						A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOWOUNDS"));
						setweaponstate("nope");
					}else setweaponstate("patchup");
					return;
				}
			}
		}
		invoker.bwimpy_weapon=true;
		int mbl=invoker.weaponstatus[MEDS_BLOOD];
		if(mbl>random(5,64)){
			invoker.weaponstatus[MEDS_BLOOD]--;
			if(mbl>random(0,255))A_SpawnItemEx(bloodtype,
				frandom(0,3),frandom(-0.3,0.3)*radius,
				height*frandom(0.,0.3),
				flags:SXF_USEBLOODCOLOR|SXF_NOCHECKPOSITION
			);
		}
	}
	states{
	select:
		TNT1 A 10{
			if(!DoHelpText()) return;
			A_WeaponMessage(Stringtable.Localize("$MEDIKIT_FIRETOHEAL"),175);
		}
		goto super::select;
	ready:
		TNT1 A 1 A_MedikitReady();
		goto readyend;
	flashstaple:
		TNT1 A 1{
			A_StartSound("medikit/staple",CHAN_WEAPON);
			A_StartSound("misc/bulletflesh",CHAN_BODY,CHANF_OVERLAP);
			invoker.weaponstatus[MEDS_BLOOD]+=random(0,2);
			if(hdplayerpawn(self)){
				HDF.Give(self,"SecondFlesh",1);
			}else givebody(3);
		}goto flashend;
	flashnail:
		TNT1 A 1{
			A_StartSound("medikit/stopper",CHAN_WEAPON,CHANF_OVERLAP);
			A_StartSound("misc/bulletflesh",CHAN_BODY,CHANF_OVERLAP);
			invoker.weaponstatus[MEDS_BLOOD]+=random(1,2);
		}goto flashend;
	flashend:
		TNT1 A 1{
			givebody(1);
			damagemobj(invoker,self,1,"staples");
			A_ZoomRecoil(0.9);
			A_ChangeVelocity(frandom(-0.2,0.03),frandom(-0.2,0.2),0.4,CVF_RELATIVE);
		}
		stop;
	altfire:
	althold:
	fireother:
		TNT1 A 0 A_JumpIf(pressingfiremode()&&!pressingzoom(),"diagnoseother");
		TNT1 A 10{
			flinetracedata mediline;
			linetrace(
				angle,radius*4,pitch,
				offsetz:height*0.8,
				data:mediline
			);
			let patient=HDPlayerPawn(mediline.hitactor);
			if(!patient){
				//resolve where the target is not an HD player
				if(
					mediline.hitactor
					&&mediline.hitactor.bsolid
					&&!mediline.hitactor.bnoblood
					&&!mediline.hitactor.bspecialfiredamage  //must see wounds to staple them
					&&(
						mediline.hitactor.bloodtype=="HDMasterBlood"
						||mediline.hitactor.bloodtype=="Blood"
					)
					&&(
						mediline.hitactor is "HDHumanoid"
					)
				){
					let mb=hdmobbase(mediline.hitactor);
					if(
						mediline.hitactor.health<mediline.hitactor.spawnhealth()
						||(
							mb
							&&mb.bodydamage>0
						)
					){
						if(invoker.weaponstatus[MEDS_SECONDFLESH]<1){
							A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOSUTURES"));
							return resolvestate("nope");
						}
						invoker.target=mediline.hitactor;
						return resolvestate("patchupother");
					}else{
						A_WeaponMessage(Stringtable.Localize("$MEDIKIT_OTHERWOUNDS"));
						return resolvestate("nope");
					}
				}else{
					if(DoHelpText())A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOTHINGTOBEDONE"),150);
					return resolvestate("nope");
				}
			}
			if(
				patient.player
				&&invoker.weaponstatus[MEDS_USEDON]>=0
				&&invoker.weaponstatus[MEDS_USEDON]!=patient.playernumber()
			){
				if(DoHelpText(patient))HDWeapon.ForceWeaponMessage(patient,string.format(Stringtable.Localize("$MEDIKIT_USEDSYRINGE"),player.getusername()));
				if(DoHelpText())A_WeaponMessage(Stringtable.Localize("$MEDIKIT_ATTACKUSED"));
			}else if(IsMoving.Count(patient)>4){
				if(DoHelpText(patient))HDWeapon.ForceWeaponMessage(patient,string.format(Stringtable.Localize("$MEDIKIT_OTHERUSEONYOU"),player.getusername()));
				if(DoHelpText())A_WeaponMessage(Stringtable.Localize("$MEDIKIT_STAYSTILL"));
				return resolvestate("nope");
			}
			let blockinv=HDWoundFixer.CheckCovered(patient,CHECKCOV_CHECKBODY);
			if(
				!patient.player.bot
				&&blockinv
			){
				if(DoHelpText())A_WeaponMessage(Stringtable.Localize("$MEDIKIT_TAKEOFF1")..blockinv.gettag()..Stringtable.Localize("$MEDIKIT_TAKEOFF2"),100);
				return resolvestate("nope");
			}
			if(
				!(getplayerinput(MODINPUT_BUTTONS)&BT_ZOOM)
				&&!hdbleedingwound.findbiggest(patient,HDBW_FINDPATCHED)
			){
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_OTHERNOWOUNDS"));
				return resolvestate("nope");
			}
			if(
				getplayerinput(MODINPUT_BUTTONS)&BT_ZOOM
				&&patient.burncount<1
			){
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_OTHERNOBURNS"));
				return resolvestate("nope");
			}
			if(invoker.weaponstatus[MEDS_SECONDFLESH]<1){
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOSUTURES"));
				return resolvestate("nope");
			}
			invoker.target=patient;
			return resolvestate("patchupother");
		}goto nope;
	patchupother:
		TNT1 A 0{
			if(
				invoker.target
				&&invoker.target.player
			)invoker.weaponstatus[MEDS_USEDON]=invoker.target.playernumber();
			else invoker.weaponstatus[MEDS_USEDON]=MEDIKIT_NOTAPLAYER;
			invoker.patientname=invoker.target.gettag();
		}
		TNT1 A 0 A_JumpIf(pressingzoom(),"patchburnsother");
		TNT1 A 10{
			invoker.weaponstatus[MEDS_SECONDFLESH]--;
			if(invoker.target){
				invoker.target.A_StartSound("medikit/stopper",CHAN_WEAPON,CHANF_OVERLAP);
				invoker.target.A_StartSound("misc/bulletflesh",CHAN_BODY,CHANF_OVERLAP);
			}
		}
		TNT1 AAAAA 3{
			let itg=invoker.target;
			
			if(
				!itg
				||absangle(angle,angleto(itg))>60
				||distance3dsquared(itg)>(radius*radius*16)
			){
				invoker.target=null;
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_TARGETNOPED"),15);
				setweaponstate("nope");
				return;
			}
			A_StartSound("medikit/staple",CHAN_WEAPON);
			invoker.weaponstatus[MEDS_BLOOD]+=random(0,1);

			itg.A_StartSound("misc/smallslop",CHAN_BODY,CHANF_OVERLAP);
			if(!random(0,3))invoker.setstatelabel("patchupend");
			itg.givebody(1);
			itg.damagemobj(invoker,null,1,"staples",DMG_FORCED);

			if(hdplayerpawn(itg)){
				HDF.Give(itg,"SecondFlesh",1);
			}else{
				if(hdmobbase(itg))hdmobbase(itg).bodydamage-=3;
				itg.givebody(3);
				hdmobbase.forcepain(itg);
			}
		}goto patchupend;
	patchup:
		TNT1 A 10;
		TNT1 A 0{
			if(invoker.weaponstatus[MEDS_SECONDFLESH]<1){
				A_WeaponMessage(Stringtable.Localize("$MEDIKIT_NOSUTURES"));
				setweaponstate("nope");
				return;
			}
			invoker.weaponstatus[MEDS_USEDON]=playernumber();
			invoker.weaponstatus[MEDS_SECONDFLESH]--;
		}
		TNT1 A 10 A_Overlay(3,"flashnail");
		TNT1 AAAAA random(4,5){
			invoker.target=self;
			A_Overlay(3,"flashstaple");
			if(!random(0,3))invoker.setstatelabel("patchupend");
		}goto patchupend;
	patchupend:
		TNT1 A 10{
			let itg=invoker.target;
			if(itg){
				let tgw=invoker.targetwound;
				if(
					!tgw
					||tgw.bleeder!=itg
				){
					tgw=hdbleedingwound.findbiggest(itg,HDBW_FINDPATCHED);
					invoker.targetwound=tgw;
				}
				if(
					tgw
					&&!tgw.depth
					&&!tgw.patched
				){
					invoker.targetwound=null;
					A_WeaponMessage(Stringtable.Localize("$MEDIKIT_WOUNDSEALED"),70);
					setweaponstate("patchdone");
					return;
				}
				if(
					tgw
					&&tgw.patch(frandom(0.8,1.2),true)
				){
					tgw.depth+=tgw.patched;
					tgw.patched=0;
				}
			}
		}
		TNT1 A 0 A_ClearRefire();
		goto ready;
	patchdone:
		TNT1 A 4;
		TNT1 A 4 A_StartSound("misc/bulletflesh",CHAN_WEAPON,CHANF_OVERLAP);
		TNT1 A 3 A_SpawnItemEx(bloodtype,
			frandom(0,3),frandom(-0.3,0.3)*radius,
			height*frandom(0.,0.3),
			flags:SXF_USEBLOODCOLOR|SXF_NOCHECKPOSITION
		);
		TNT1 A 2;
		goto nope;
	patchburns:
		TNT1 A 6;
		TNT1 A 8{
			if(!HDPlayerPawn(self))return;
			invoker.weaponstatus[MEDS_BLOOD]+=random(1,2);
			invoker.weaponstatus[MEDS_USEDON]=playernumber();
			int fleshgive=min(MEDIKIT_FLESHGIVE,invoker.weaponstatus[MEDS_SECONDFLESH]);
			invoker.weaponstatus[MEDS_SECONDFLESH]-=fleshgive;
			A_StartSound("medikit/stopper",CHAN_WEAPON);
			A_StartSound("misc/bulletflesh",CHAN_BODY,CHANF_OVERLAP);
			A_StartSound("misc/smallslop",CHAN_BODY,CHANF_OVERLAP);
			actor a=spawn("SecondFleshBeast",pos,ALLOW_REPLACE);
			a.target=self;
			a.stamina=fleshgive;
		}
		goto ready;
	patchburnsother:
		TNT1 A 10{
			if(invoker.target){
				invoker.weaponstatus[MEDS_BLOOD]+=random(1,2);
				int fleshgive=min(MEDIKIT_FLESHGIVE,invoker.weaponstatus[MEDS_SECONDFLESH]);
				invoker.weaponstatus[MEDS_SECONDFLESH]-=fleshgive;
				invoker.target.A_StartSound("medikit/stopper",CHAN_WEAPON);
				invoker.target.A_StartSound("misc/bulletflesh",CHAN_BODY,CHANF_OVERLAP);
				invoker.target.A_StartSound("misc/smallslop",CHAN_BODY,CHANF_OVERLAP);
				actor a=spawn("SecondFleshBeast",invoker.target.pos,ALLOW_REPLACE);
				a.target=invoker.target;
				a.stamina=fleshgive;
			}
		}
		goto nope;

	diagnose:
		TNT1 A 0 A_WeaponMessage(Stringtable.Localize("$MEDIKIT_AUTODIAGNOSTIC"));
		TNT1 AAAAAAAAAAAA 2{
			A_StartSound("medikit/scan",CHAN_WEAPON,volume:0.5);
			A_SetBlend("aa aa 88",0.04,1);
		}
		TNT1 A 0 A_ScanResults(self,12);
		TNT1 A 0 A_Refire("nope");
		goto readyend;
	diagnoseother:
		TNT1 A 0{
			A_WeaponMessage(Stringtable.Localize("$MEDIKIT_AUTODIAGNOSTIC"));
			invoker.target=null;
			invoker.weaponstatus[MEDS_ACCURACY]=0;
		}
		TNT1 AAAAAAAAAAAA 2{
			A_StartSound("medikit/scan",CHAN_WEAPON,volume:0.4);
			flinetracedata mediline;
			linetrace(
				angle,42,pitch,
				offsetz:height-12,
				data:mediline
			);
			let mha=mediline.hitactor;
			if(
				!mha
				||(invoker.target&&mha!=invoker.target)
			){
				invoker.target=null;
				invoker.weaponstatus[MEDS_ACCURACY]=0;
				return;
			}
			invoker.target=mha;
			invoker.weaponstatus[MEDS_ACCURACY]++;
		}
		TNT1 A 0 A_ScanResults(invoker.target,invoker.weaponstatus[MEDS_ACCURACY]);
		TNT1 A 0 A_Refire("nope");
		goto readyend;

	spawn:
		MEDI B -1 nodelay{
			if(
				invoker.weaponstatus[MEDS_USEDON]>=0
			){
				frame=2;
				if(invoker.weaponstatus[MEDS_BLOOD]>0){
					actor bbb=spawn("BloodSplatSilent",pos,ALLOW_REPLACE);
					if(bbb)bbb.vel=vel;
					tics=random(10,500-invoker.weaponstatus[MEDS_BLOOD]);
					invoker.weaponstatus[MEDS_BLOOD]--;
				}
			}
		}wait;
	}
	action void A_ScanResults(actor scanactor,double scanaccuracy){
		A_StartSound("medikit/done",CHAN_WEAPON);
		double thrownoff=scanaccuracy-12;
		if(!scanactor||abs(thrownoff)>10){
			A_WeaponMessage(Stringtable.Localize("$MEDIKIT_AUTODIAGNOSTICFAIL"));
			invoker.target=null;
			invoker.weaponstatus[MEDS_ACCURACY]=0;
			return;
		}

		if(HDWoundFixer.CheckCovered(scanactor,CHECKCOV_CHECKBODY))thrownoff+=5.;

		string playerspecs="";
		let slf=HDPlayerPawn(scanactor);
		if(slf){
			thrownoff+=frandom(0,slf.aggravateddamage);
			string bloodloss=string.format("%.2f",
				double(slf.bloodloss)/(HDCONST_BLOODBAGAMOUNT<<2)
				+frandom(0,thrownoff*0.2)
			);
			playerspecs=Stringtable.Localize("$MEDIKIT_BURNS")..slf.burncount..Stringtable.Localize("$MEDIKIT_TISSUEDAMAGE")..slf.oldwoundcount..Stringtable.Localize("$MEDIKIT_BLOODLOSS")..bloodloss..Stringtable.Localize("$MEDIKIT_TRANSFUSIONUNITS");
		}

		string openwounds="";
		string bandaged="";
		string treated="";
		hdbleedingwound bldw=null;
		thinkeriterator bldit=thinkeriterator.create("HDBleedingWound");
		int rowcount=0;
		while(bldw=HDBleedingWound(bldit.next())){
			if(
				bldw
				&&bldw.bleeder==scanactor
			){
				if(
					!bldw.depth+frandom(-thrownoff,thrownoff)
					&&!bldw.patched
					&&!bldw.healing
				)continue;

				string ams=
					(rowcount?"  ":"\n")
					.."\cg"..string.format("%.1f",bldw.depth).."\cc/\ck"..
					string.format("%.1f",bldw.patched+frandom(-thrownoff,thrownoff)).."\cc/\cu"..
					string.format("%.1f",bldw.healing)
				;
				openwounds=openwounds.." "..ams;

				if(rowcount==2)rowcount=0;else rowcount++;
			}
		}
		if(openwounds=="")openwounds=Stringtable.Localize("$MEDIKIT_NONE");


		A_WeaponMessage(Stringtable.Localize("$MEDIKIT_AUTODIAGNOSTICFOR")..scanactor.gettag()..Stringtable.Localize("$MEDIKIT_WOUNDS")..openwounds..""..playerspecs,270);

		A_Log(Stringtable.Localize("$MEDIKIT_AUTODIAGNOSTICFOR")..scanactor.gettag()..Stringtable.Localize("$MEDIKIT_WOUNDS")..openwounds..playerspecs,true);
	}
}
class SecondFleshBeast:IdleDummy{
	states{
	spawn:
		TNT1 A 14;
		TNT1 A 16{target.A_StartSound("medikit/crackle",CHAN_BODY,CHANF_OVERLAP);}
		TNT1 A 0{
			target.A_StartSound("medikit/crackle",CHAN_BODY,CHANF_OVERLAP);
			target.A_Scream();
			let tgt=HDPlayerPawn(target);
			if(tgt)tgt.AddBlackout(128,24,4);
		}
		TNT1 AAA 4{
			let tgt=HDPlayerPawn(target);
			if(tgt){
				tgt.muzzleclimb1+=(frandom(-4,4),frandom(-10,12));
				tgt.muzzleclimb2+=(frandom(-4,4),frandom(-10,12));
				tgt.muzzleclimb3+=(frandom(-4,4),frandom(-10,12));
				tgt.muzzleclimb4+=(frandom(-4,4),frandom(-10,12));
			}
		}
		TNT1 A 4{
			let tgt=HDPlayerPawn(target);
			if(!tgt||tgt.bkilled||stamina<1){destroy();return;}
			if(tgt.health>10)tgt.damagemobj(tgt,tgt,min(tgt.health-10,3),"internal",DMG_NO_ARMOR);
			if(tgt)tgt.AddBlackout(24,8,4);
			tgt.A_StartSound("medikit/crackle",CHAN_BODY,CHANF_OVERLAP);
			tgt.muzzleclimb1+=(frandom(-1,1),frandom(-1,1));
			tgt.muzzleclimb2+=(frandom(-1,1),frandom(-1,1));
			tgt.muzzleclimb3+=(frandom(-1,1),frandom(-1,1));
			tgt.muzzleclimb4+=(frandom(-1,1),frandom(-1,1));
			tics=clamp(200-stamina,4,random(4,40));
			if(tics>15)tgt.A_StartSound(tgt.painsound,CHAN_VOICE);
			tgt.stunned+=10;
			tgt.burncount--;
			if(!random(0,200))tgt.aggravateddamage++;
			stamina--;
			if(hd_debug)A_Log(string.format("aggro %i  old %i  burn %i",tgt.aggravateddamage,tgt.oldwoundcount,tgt.burncount));
		}wait;
	}
}
class SecondFlesh:HDDrug{
	override void PreTravelled(){
		let hdp=hdplayerpawn(owner);
		hdp.burncount=max(0,hdp.burncount-amount);
		amount=0;
	}
	override void OnHeartbeat(hdplayerpawn hdp){
		if(amount<1)return;
		int amt=amount;

		if(
			hdp.health>hdp.healthcap-(amt>>2)
		)hdp.damagemobj(self,hdp,1,"maxhpdrain");

		if(hdp.fatigue<random(1,amt))hdp.fatigue++;

		if(hdp.beatcounter%12==0){
			amount--;
			if(
				hdp.oldwoundcount>0
				&&random(0,2)
			)hdp.oldwoundcount--;

			double healamount=frandom(0.1,3.);
			array<hdbleedingwound> wounds;wounds.clear();
			HDBleedingWound bldw=null;
			thinkeriterator bldit=thinkeriterator.create("HDBleedingWound");
			while(bldw=HDBleedingWound(bldit.next())){
				if(
					bldw
					&&bldw.bleeder==hdp
					&&bldw.healing>0
				)wounds.push(bldw);
			}
			if(wounds.size()>0){
				healamount/=wounds.size();
				for(int i=0;i<wounds.size();i++){
					wounds[i].healing=max(0,wounds[i].healing-healamount);
				}
			}

			if(!random(0,47))hdp.aggravateddamage++;
			damagemobj(self,hdp,1,"staples");
		}

		if(hd_debug>=4)console.printf("2FLS "..amt.."  = "..hdp.burncount);
	}
}




//-------------------------------------------------
// Alternate medikit for nobleed mode
//-------------------------------------------------
class HDMedikitterNoBleed:PortableStimpack{
	default{
		//$Category "Items/Hideous Destructor/Supplies"
		//$Title "Medikit"
		//$Sprite "MEDIA0"

		-hdweapon.droptranslation
		inventory.pickupmessage "$PICKUP_MEDIKIT";
		inventory.icon "MEDIB0";
		scale 0.4;
		tag "$TAG_MEDIKIT";
		hdweapon.refid HDLD_MEDIKIT;
		+inventory.ishealth
		+hdweapon.norandombackpackspawn

		weapon.selectionorder 1002;

		portablestimpack.mainhelptext "$MEDINOBLEED_MAINHELPTEXT";
		portablestimpack.spentinjecttype "SpentMedi";
		portablestimpack.injecttype "InjectMediNoBleedDummy";
	}
	override double weaponbulk(){
		return ENC_MEDIKIT;
	}
	action void A_CheckNoBleed(){
		if(!hd_nobleed){
			A_GiveInventory("HDMedikitter");
			invoker.destroy();
			if(!hdplayerpawn(self)||!hdplayerpawn(self).incapacitated){
				HDWeaponSelector.Select(self,"HDMedikitter",1);
			}
			ReplaceSpares(self);
		}
	}
	static void ReplaceSpares(actor caller){
		let spw=SpareWeapons(caller.findinventory("SpareWeapons"));
		if(!spw)return;
		string mdk1="HDMedikitter";
		string mdk2="HDMedikitter";
		if(hd_nobleed)mdk2=mdk2.."NoBleed";
		else mdk1=mdk1.."NoBleed";
		int spwsize=spw.weapontype.size();
		for(int i=0;i<spwsize;i++){
			spw.weapontype[i].replace(mdk1,mdk2);
		}
	}
	states{
	spawn2:
		MEDI B -1;
		stop;
	fire:
	hold:
		TNT1 A 0 A_CheckNoBleed();
		goto super::fire;
	altfire:
	althold:
		TNT1 A 0 A_CheckNoBleed();
		goto super::altfire;
	}
}
class InjectMediNoBleedDummy:InjectStimDummy{
	states{
	spawn:
		TNT1 A 10;
		TNT1 A 1{
			if(!target||target.bkilled){destroy();return;}
			let hdp=HDPlayerPawn(target);
			if(hdp){
				hdp.bloodloss=0;
				hdp.burncount=0;
				hdp.oldwoundcount=0;
				hdp.stunned=50;
				hdp.givebody((HDCONST_JITTERHEALTH+20)-hdp.health);
				hdp.damagemobj(
					self,null,hdp.health-HDCONST_JITTERHEALTH,"staples",
					DMG_NO_ARMOR|DMG_PLAYERATTACK|DMG_NO_FACTOR|DMG_FORCED
				);
			}else{
				target.givebody(int(target.spawnhealth()*0.8));
				HDMobBase.forcepain(target);
			}
		}stop;
	}
}
class SpentMedi:SpentStim{
	default{
		translation "none";
		scale 0.4;
	}
	states{
	spawn:
		MEDI C 0;
		goto spawn2;
	}
}
