# Hideous Destructor
A gameplay modification for GZDoom https://github.com/ZDoom/gzdoom that adds more detailed and (arguably) realistic movement, physics, weapon behaviour and monster attacks.

To use (assuming you are reading this on the Git page):
1. Download the repository as a zip file.
2. Drag that file into GZDoom.exe or select it using the GZDoom command line or your favourite launcher.

If you are reading this after having pulled it out of the already-downloaded zip/folder, load that zip/folder into GZDoom.

If you are downloading the release, just load the .pk7 the same way you do a .pk3, and open it as a .7z archive to access the manual. 

Make sure you have keys bound for speed, crouch, reload, zoom, and user1 through 4.

---

hd_manual.md contains the full manual.

hd.txt contains a draft of the relevant information for any future archival in /idgames.

