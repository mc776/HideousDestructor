//for id


//FlySpitter
object FlySpitter{
	frame PAIN { light HDIMPBALL }
	frame PAINF { light ROQX2 }
	frame PAINH { light ROQX2 }
	frame PAINI { light ROQX1 }
	frame PAINJ { light ROQX1 }
	frame PAINK { light ROQX2 }
	frame PAINL { light ROQX2 }
	frame PAINM { light ROQX3 }
}
object FlyingSkull{
	frame SKUL { light HDIMPBALL }
	frame SKULC { light HDIMPBX1 }
	frame SKULD { light HDIMPBX1 }
	frame SKULF { light ROQX2 }
	frame SKULG { light ROQX3 }
	frame SKULH { light HDIMPBX1 }
	frame SKULI { light HDIMPBX2 }
	frame SKULJ { light HDIMPBX3 }
	frame SKULK { light HDIMPBX3 }
}


//make bfg green instead of blue
flickerlight BFS1{
	color 0.1 1 0.3
	size 36
	secondarysize 24
	chance 0.5
}
pointlight BFE1A{
	color 0.3 1 0.6
	size 36
}
pointlight BFE1B{
	color 0.5 1 0.8
	size 56
}
pointlight BFE1C{
	color 0.8 1 0.95
	size 86
}


//make health potion blue again
pulselight HEALTHPOTION{
	color 0 0 0.3
	size 12
	secondarysize 6
	interval 0.1
}

